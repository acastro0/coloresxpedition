import { Component, Input, OnInit } from '@angular/core';
import { ApiColoresService } from '../../servicios/api-colores.service';
import { Color } from "../../modelos/color";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-vercolor',
  templateUrl: './vercolor.component.html',
  styleUrls: ['./vercolor.component.css']
})
export class VercolorComponent implements OnInit {
  public color: Color= {id: 0,name: "", year: 0, color: "", pantone_value: ""};
  public colores:Array<Color> = []
  pagina: number=1
  fondo=''
  constructor(private apiservice: ApiColoresService) { }

  ngOnInit(): void {
    this.cargarApi();
    this.cargarFondoCarta();
  }
  cargarApi(){
    this.apiservice.getColores(this.pagina).subscribe(colores =>{
      (this.colores = colores.data)
    } );
  }
  cargarFondoCarta(){
    this.fondo= this.color.color
  }
  anteriorPagina(){
    if(this.pagina=1){
    }else{
      this.pagina=this.pagina-1
    }
    this.ngOnInit()
  }
  siguientePagina(){
    if(this.pagina=2){
    }else{
      this.pagina=this.pagina+1
    }
    this.ngOnInit()
  }
  mostrarNotificacion(){
    Swal.fire(
      'Color Copiado!',
      '',
      'success'
    )
  }

}
