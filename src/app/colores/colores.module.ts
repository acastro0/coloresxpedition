import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VercolorComponent } from './componentes/vercolor/vercolor.component';
import { HeaderComponent } from './plantillas/header/header.component';
import { AppRoutingModule } from '../app-routing.module';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { FooterComponent } from './plantillas/footer/footer.component';


@NgModule({
  declarations: [
    VercolorComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    ClipboardModule
  ],
  exports: [
    VercolorComponent,
    HeaderComponent,
    AppRoutingModule,
    FooterComponent
  ],
})
export class ColoresModule { }
