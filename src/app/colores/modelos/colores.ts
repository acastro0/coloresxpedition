import { Color } from "./color";

export interface Colores{
  page: number
  per_page:number
  total:number
  total_pages:number
  data: Array<Color>
}
