import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Colores } from '../modelos/colores';


@Injectable({
  providedIn: 'root'
})

export class ApiColoresService {

  private url = "https://reqres.in/api/colors"

  constructor(private http: HttpClient) {}

  public getColores(pag: number): Observable<Colores>{
    return this.http.get<Colores>(this.url+"?page="+pag)
  }


}
