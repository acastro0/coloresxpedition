import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ColoresModule } from "./colores/colores.module";
import { VercolorComponent } from './colores/componentes/vercolor/vercolor.component';
import { HttpClientModule} from '@angular/common/http';

const routes: Routes = [
  { path: '', component: VercolorComponent, pathMatch:'full' },
  { path: '**', redirectTo:'/',pathMatch: 'full' },
]

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ColoresModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
