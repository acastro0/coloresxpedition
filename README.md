# ColoresXpedition

Esta aplicacion permite tomar la informacion de la API facilitada, ordenandola en tarjetas que al ser seleccionadas copian el codigo HEX de color seleccionado en el portapapeles

## Tecnologias usadas

-Angular Cli
<p>
-Bootstrap
<p>
-Node
<p>
-Gitlab
<p>
-Netlify

## Dependencias e instalacion

Se usaron adicionalmente las siguientes dependencias no incluidas por defecto en Angular:
<p>
<details>
<summary>Cdk</summary>

Para poder instalar Cdk se puede usar <b>NPM</b>

<pre><code>npm install @angular/cdk@9.0.0-rc.8</code></pre>

</p>

<p>
<details>
<summary>sweetalert2</summary>

Para poder instalar sweetalert2 se puede usar <b>NPM</b>

<pre><code>npm install sweetalert2</code></pre>

</p>
<p>
<details>
<summary>RxJS</summary>

Para poder instalar RxJS se puede usar <b>NPM</b> 

<pre><code>npm install --save rxjs-compat</code></pre>
</p>

## Ejecutar proyecto

Para poder instalar RxJS se usa el siguente comando que ejecutara el proyecto en la direccion 'http://localhost:4200'

<pre><code>ng serve -o</code></pre>

## Links

<b>Gitlab:</b> https://gitlab.com/acastro0/coloresxpedition

<p>
<b>Netlify:</b> https://nimble-kataifi-dc8835.netlify.app


